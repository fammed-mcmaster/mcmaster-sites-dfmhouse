Introduction
============

This add-on provides the necessary dependencies and customizations to
for a Plone site to have the branding and behaviour of the DFMHouse.ca
website.
