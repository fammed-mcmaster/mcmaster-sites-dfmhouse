"""
Interfaces specific to this add-on.
"""

from plone.theme.interfaces import IDefaultPloneLayer

class IDFMHouseLayer(IDefaultPloneLayer):
    """A marker interface specific to DFMHouse."""
