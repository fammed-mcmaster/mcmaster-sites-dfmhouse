"""Setup code for DFMHouse.
"""

from Products.CMFCore.utils import getToolByName
from Products.ATVocabularyManager.utils.vocabs import createHierarchicalVocabs


def createVocabulariesInFreshInstall(context):
    """Create the vocabularies in a fresh install.
    """

    if context.readDataFile('dfmhouse.txt') is None:
        return

    # context is a Products.GenericSetup.context.DirectoryImportContext
    createVocabularies(context.getSite())


def createVocabularies(context):
    """Creates vocabularies needed by DFMHouse site.
    """

    portal_vocabularies = getToolByName(context, 'portal_vocabularies')
    
    if not hasattr(portal_vocabularies, 'fm_accreditation'):
        portal_vocabularies.invokeFactory('SimpleVocabulary',
                                          'fm_accreditation',
                                          title='Accreditation')
        portal_vocabularies['fm_accreditation'].reindexObject()

    dfm_vocabularies = {
        ('fm_locations', 'Locations'): {},
        ('fm_expertise', 'Expertise'): {},
        }
    createHierarchicalVocabs(portal_vocabularies,
                             dfm_vocabularies)
