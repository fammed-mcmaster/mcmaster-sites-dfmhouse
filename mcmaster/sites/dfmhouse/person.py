"""
FSD Person customizations.
"""

from zope.interface import implements
from zope.component import adapts
from archetypes.schemaextender.interfaces import (
    ISchemaModifier,
    IBrowserLayerAwareExtender,
    )
from archetypes.schemaextender.field import ExtensionField
from Products.Archetypes.public import StringField
from Products.Archetypes.public import LinesField
from Products.Archetypes.public import StringWidget
from Products.FacultyStaffDirectory.interfaces.person import IPerson
from Products.ATVocabularyManager import NamedVocabulary
from collective.dynatree.atwidget import DynatreeWidget
from Products.AddRemoveWidget import AddRemoveWidget

from mcmaster.sites.dfmhouse.browser.interfaces import IDFMHouseLayer


class _StringExtensionField(ExtensionField, StringField):
    pass


class _LinesExtensionField(ExtensionField, LinesField):
    pass


class PersonModifier(object):
    """Several customizations for FSD's Person needed by the DFMHouse
    site."""

    adapts(IPerson)
    implements(
        ISchemaModifier,
        IBrowserLayerAwareExtender,
        )
    layer = IDFMHouseLayer

    _fields = [
        _LinesExtensionField(
            'locations',
            required=False,
            searchable=True,
            vocabulary=NamedVocabulary("fm_locations"),
            schemata="Basic Information",
            widget=DynatreeWidget(
                label=u"Locations",
                description=u'Select the locations you are affiliated to.',
                selectMode=2,
                leafsOnly=True,
                ),
            ),

        _StringExtensionField(
            'faxNumber',
            required=False,
            searchable=True,
            schemata="Contact Information",
            widget=StringWidget(
                label=u"Fax number",
                ),
            ),

        _StringExtensionField(
            'mobilePhone',
            required=False,
            searchable=True,
            schemata="Contact Information",
            widget=StringWidget(
                label=u"Mobile phone",
                ),
            ),

        _StringExtensionField(
            'pager',
            required=False,
            searchable=True,
            schemata="Contact Information",
            widget=StringWidget(
                label=u"Pager",
                ),
            ),

        _LinesExtensionField(
            'expertise',
            required=False,
            searchable=True,
            vocabulary=NamedVocabulary("fm_expertise"),
            schemata="Professional Information",
            widget=DynatreeWidget(
                    label=u"Expertise",
                    selectMode=2,
                    leafsOnly=True,
                    ),
            ),

        _LinesExtensionField(
            'accreditation',
            required=False,
            searchable=True,
            vocabulary=NamedVocabulary("fm_accreditation"),
            schemata="Professional Information",
            widget=AddRemoveWidget(
                label=u"Accreditation",
                allow_add=0,
                width='15em',
               ),
            ),
        ]

    def __init__(self, context):
        self.context = context

    def fiddle(self, schema):
        """Hide unneeded fields, add needed ones.
        """

        # Set unwanted fields to invisible in all modes, deleting them
        # is not advisable as some code/template might be referring to
        # either of them.
        for unwanted in ['suffix',
                         'departments',
                         'committees',
                         'specialties',
                         'education',
                         'websites',
                         ]:
            new_field = schema[unwanted].copy()
            new_field.widget.visible = False
            schema[unwanted] = new_field

        # Custom labels in the form (fieldName, newLabel)
        custom_labels = [
            ('officeAddress', u'Address', None),
            ('officeCity', u'City', None),
            ('officeState', u'Province', None),
            ('officePostalCode', u'Postal code', None),
            ('officePhone', u'Phone', None),
            ('biography', u'Role/Function', u'Give us a few sentences about yourself.'),
            ]
        for (field_name, new_label, new_description) in custom_labels:
            new_field = schema[field_name].copy()
            new_field.widget.label = new_label
            if new_description is not None:
                new_field.widget.description = new_description
            schema[field_name] = new_field

        for field in self._fields:
            schema.addField(field)

        return schema
