from setuptools import setup, find_packages
import os

def read(*rnames):
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()

version = read("mcmaster",
               "sites",
               "dfmhouse",
               "version.txt")

setup(name='mcmaster.sites.dfmhouse',
      version=version,
      description="DFMHouse.ca site policies and customizations.",
      long_description=read('README.txt') + '\n' + read('docs', 'HISTORY.txt'),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone",
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        "Programming Language :: Python",
        ],
      keywords='',
      author='Servilio Afre Puentes',
      author_email='afrepues@mcmaster.ca',
      url='',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['mcmaster', 'mcmaster.sites'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
          'Products.CMFPlone>=4.0,<5',
          "Products.FacultyStaffDirectory",
          'archetypes.schemaextender',
          'collective.fsdsimplifier',
          'collective.dynatree',
          'Products.ATVocabularyManager',
          'Products.AddRemoveWidget',
          'collective.geo.bundle',
      ],
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
